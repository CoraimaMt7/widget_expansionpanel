import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(title: 'Peliculas de Anime'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool peliculas = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              ExpansionPanelList(
                animationDuration: Duration(seconds: 1),
                expansionCallback: (int item, bool status) {
                  print(item); //status>item
                  // solo con StatefulWidget
                  setState(() {
                    this.peliculas = !status;
                  });
                },
                children: <ExpansionPanel>[
                  ExpansionPanel(
                    headerBuilder: (context, bool) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 15),
                        child: Container(
                          child: Text(
                            'Your Name (2016)',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      );
                    }, //Cabecera
                    body: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        child: Image.network(
                            "https://2.bp.blogspot.com/-rr6wK8ndk1U/WOZ4NAMeeBI/AAAAAAABVEw/iYxmFAw4ZzoIlQvve_u89itzjzqloKtTQCLcB/s1600/your%2Bname.jpg"),
                      ),
                    ),
                    //Expandir
                    isExpanded: this.peliculas,
                  ),
                  ExpansionPanel(
                    headerBuilder: (context, bool) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 15),
                        child: Container(
                          child: Text(
                            'El tiempo contigo (2019)',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                      );
                    }, //Cabecera
                    body: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        child: Image.network("https://i.imgur.com/f6AwjDh.jpg"),
                      ),
                    ),
                    //Expandir
                    isExpanded: this.peliculas,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
